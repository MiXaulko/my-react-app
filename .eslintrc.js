module.exports = {
  extends: ['airbnb', 'prettier', 'prettier/react'],
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  plugins: ['react', 'react-hooks', 'prettier'],
  parser: 'babel-eslint',
  rules: {
    'react-hooks/rules-of-hooks': 'error',
    'no-console': 'error',
    'jsx-a11y/alt-text': 'off',
    'import/no-extraneous-dependencies': 'off',
    'import/prefer-default-export': 'off',
    'react/prop-types': 'off',
    'import/no-default-export': 'error',
    "linebreak-style": ["error", "windows"],
    curly: ['error', 'all'],
    'react/destructuring-assignment': 'off',
    'react/jsx-filename-extension': ['error', {extensions: ['.js', '.jsx']}],
  },
};
