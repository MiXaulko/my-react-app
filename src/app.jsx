import React from 'react';
import styled from 'styled-components';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { GlobalStyle } from './global-style';
import { DictionaryForm } from './components/dictionaryForm';
import { SlideMenuWithRouter } from './components/header';
import { ProductionForm } from './components/productionForm';
import { PartnerForm } from './components/partnerForm';
import { LoginFormWithRouter } from './components/loginForm';

const FetchInterceptor = require('fetch-interceptor');

const Container = styled.div`
  min-width: 100%;
  min-height: 100%;
  background: #fafafa;
`;

export const baseUrl = `http://localhost:8080/rest/`;

const interceptor = FetchInterceptor.register({
  onBeforeRequest(request, controller) {
    // Hook before request
  },
  onRequestSuccess(response, request, controller) {
    // Hook on response success
  },
  onRequestFailure(response, request, controller) {
    console.log(`intersept failure`);
    console.log(response)
    if (response.status === 403) {
      sessionStorage.clear();
      const loginPage = `http://localhost:8081/login`;
      if (window.location.href !== loginPage) {
        window.location.href = loginPage;
      }
    }
  },
});

const areatype = () => {
  return <DictionaryForm baseUrl={baseUrl}
    nameTable="Вид площади"
    nameColumn={[{ name: 'Название вида площади' }]}
    url="areatype"
    isDeletable />;
};

const workertype = () => {
  return <DictionaryForm baseUrl={baseUrl}
    nameTable="Вид работника"
    nameColumn={[{ name: 'Название вида работника' }]}
    url="workertype"
    isDeletable />;
};

const producttype = () => {
  return <DictionaryForm baseUrl={baseUrl}
    nameTable="Вид продукции"
    nameColumn={[{ name: 'Название вида продукции' }]}
    url="producttype"
    isDeletable />;
};

const ownershiptype = () => {
  return <DictionaryForm baseUrl={baseUrl}
    nameTable="Форма собственности"
    nameColumn={[{ name: `Название типа собственности` }]}
    url="ownershiptype"
    isDeletable />;
};

const industrytype = () => {
  return <DictionaryForm baseUrl={baseUrl}
    nameTable="Тип отрасли" nameColumn={[{ name: 'Название типа отрасли' }]}
    url="industrytype"
    isDeletable />;
};

const partner = () => {
  return <PartnerForm baseUrl={baseUrl} nameTable="Контрагент"
    nameColumn={[{ name: 'Название контрагента' }, { name: 'Местонахождение' }]}
    url="partner"
    isDeletable />;
};

const production = () => {
  return (
    <ProductionForm baseUrl={baseUrl} />
  );
};

const updateToken = (token) => {
  sessionStorage.setItem('token', token);
};

const login = () => {
  return (
    <LoginFormWithRouter baseUrl={baseUrl} url="auth" action={updateToken} />
  );
};

export const App = () => (
  <Container>
    <Router>
      <SlideMenuWithRouter />
      <Route path="/areatype" component={areatype} />
      <Route path="/workertype" component={workertype} />
      <Route path="/producttype" component={producttype} />
      <Route path="/ownershiptype" component={ownershiptype} />
      <Route path="/industrytype" component={industrytype} />
      <Route path="/production" component={production} />
      <Route path="/partner" component={partner} />
      <Route path="/login" component={login} />
    </Router>
    <GlobalStyle />
  </Container>
);
