import { baseUrl } from '../app';

export class HttpService {
  href;

  constructor(href) {
    this.href = baseUrl + href;
  }

  getSimpleToken() {
    return new Headers({ 'Authorization': sessionStorage.getItem('token') });
  }

  getJsonToken() {
    return new Headers({ 'Content-type': 'application/json', 'Authorization': sessionStorage.getItem('token') });
  }

  get() {
    const header = { method: 'GET', headers: this.getSimpleToken() };
    return fetch(this.href, header).then(r => r.json());
  }

  delete(id) {
    const header = {
      method: 'DELETE', headers: this.getSimpleToken()
    }
    return fetch(`${this.href}?id=${id}`, header);
  }

  deleteWithBody(body) {
    const header = {
      method: 'DELETE', headers: this.getJsonToken(), body
    }
    return fetch(`${this.href}`, header);
  }

  create(object) {
    const header = { method: 'POST', headers: this.getJsonToken(), body: object };
    return fetch(this.href, header);
  }

  update(object) {
    const header = { method: 'PATCH', headers: this.getJsonToken(), body: object };
    return fetch(this.href, header);
  }
}
