import React from 'react';
import {Modal} from 'react-bootstrap';
import {SubmitButton} from './buttons/submitButton';
import {CancelButton} from './buttons/cancelButton';
import {HttpService} from './httpService';

export class PopupForCreateProduction extends React.Component {
  constructor(props) {
    super(props);
    this.getIndustryTypes();
    this.getOwnershipType();

  }

  state = {
    name: '',
    phone: '',
    address: '',
    fax: '',
    industryType: [],
    ownershipType: [],
    industryTypeId: -1,
    ownershipTypeId: -1,
    service1: new HttpService(`industrytype`),
    service2: new HttpService(`ownershiptype`),
  };

  updateName = (e) => {
    this.setState({name: e.target.value});
  };

  updateAddress = (e) => {
    this.setState({address: e.target.value});
  };

  updatePhone = (e) => {
    this.setState({phone: e.target.value});
  };

  updateFax = (e) => {
    this.setState({fax: e.target.value});
  };

  returnParameters = () => {
    let {name, phone, address, fax, ownershipTypeId, industryTypeId} = this.state;
    const {production} = this.props;
    if (industryTypeId === -1) {
      industryTypeId = this.state.industryType[0].id;
    }
    if (ownershipTypeId === -1) {
      ownershipTypeId = this.state.ownershipType[0].id;
    }
    if (name === '') {
      name = production.name;
    }
    if (phone === '') {
      phone = production.phone;
    }
    if (address === '') {
      address = production.address;
    }
    if (fax === '') {
      fax = production.fax;
    }

    this.props.performAction(name, phone, address, fax, industryTypeId, ownershipTypeId);
  };

  getIndustryTypes = () => {
    this.state.service1.get()
      .then((result) => {
        this.setState({
          industryType: result,
        });
      });
  };

  getOwnershipType = () => {
    this.state.service2.get()
      .then((result) => {
        this.setState({
          ownershipType: result,
        });
      });
  };

  selectOwnershipType = (e) => {
    this.setState({ownershipTypeId: Number(e.target.value)});
  };

  selectIndustryType = (e) => {
    this.setState({industryTypeId: Number(e.target.value)});
  };

  render() {
    const {isUpdate, production, isOpen, closePopup} = this.props;
    const {industryType, ownershipType} = this.state;
    return (

      <Modal show={isOpen} onHide={closePopup}>
        <Modal.Header closeButton>
          <Modal.Title>{isUpdate ? 'Обновить' : 'Добавить'} запись</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <table>
            <tr>
              <td>
                Имя
              </td>
              <td>
                <input
                  id="name"
                  onChange={this.updateName}
                  defaultValue={isUpdate ? production.name : ''}
                />
              </td>
            </tr>
            <tr>
              <td>
                Адрес
              </td>
              <td>
                <input
                  id="address"
                  onChange={this.updateAddress}
                  defaultValue={isUpdate ? production.address : ''}
                />
              </td>
            </tr>
            <tr>
              <td>
                Телефон
              </td>
              <td>
                <input
                  id="phone"
                  onChange={this.updatePhone}
                  defaultValue={isUpdate ? production.phone : ''}
                />
              </td>
            </tr>
            <tr>
              <td>
                Факс
              </td>
              <td>
                <input
                  id="fax"
                  onChange={this.updateFax}
                  defaultValue={isUpdate ? production.fax : ''}
                />
              </td>
            </tr>
            <tr>
              <td>
                Тип отрасли
              </td>
              <td><select onChange={this.selectIndustryType}>
                {industryType.map((elem) => (
                  <option value={elem.id}>{elem.name}</option>
                ))}
              </select>
              </td>
            </tr>
            <tr>
              <td>
                Форма собственности
              </td>
              <td>
                <select onChange={this.selectOwnershipType}>
                  {ownershipType.map((elem) => (
                    <option value={elem.id}>{elem.name}</option>
                  ))}
                </select>
              </td>
            </tr>
          </table>
        </Modal.Body>
        <Modal.Footer>
          <CancelButton action={closePopup} text="Отмена"/>
          <SubmitButton action={this.returnParameters} text="Выполнить"/>
        </Modal.Footer>
      </Modal>
    );
  }
}
