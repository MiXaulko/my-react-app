import React from 'react';
import {Modal} from 'react-bootstrap'
import {SubmitButton} from './buttons/submitButton';
import {CancelButton} from './buttons/cancelButton';


export class PopupForPartner extends React.Component {
  state = {newName: '', newLocation: null};

  updateName = (e) => {
    this.setState({newName: e.target.value, newLocation: ''});
  };

  updateNameLocation = (e) => {
    this.setState({newLocation: e.target.value});
  };

  returnParameters = () => {
    const {newName, newLocation} = this.state;
    const {name, nameLocation} = this.props;
    let curName = newName;
    if (newName === '') {
      curName = name;
    }

    if (curName !== '') {
      this.props.performAction(curName, newLocation);
    } else {
      alert('Имя не должно быть пустым');
    }
  };

  render() {
    const {isUpdate, name, nameLocation, isOpen, closePopup} = this.props;
    return (
      <Modal show={isOpen} onHide={closePopup}>
        <Modal.Header closeButton>
          <Modal.Title>{isUpdate ? 'Обновить' : 'Добавить'} строку</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {isUpdate && `Текущая запись ${name}`}
          {(isUpdate && nameLocation !== null) && `, местонахождение: ${nameLocation}`}
          {isUpdate && <br/>}
          <table>
            <tr>
              <td>
                Новое имя
              </td>
              <td>
                <input
                  id="name"
                  onChange={this.updateName}
                  defaultValue={name}
                />
              </td>
            </tr>

            <tr>
              <td>
                Новое местонахождение
              </td>
              <td>
                <input
                  id="nameLocation"
                  onChange={this.updateNameLocation}
                  defaultValue={nameLocation}
                />
              </td>
            </tr>
          </table>
        </Modal.Body>
        <Modal.Footer>
          <CancelButton action={closePopup} text="Отмена"/>
          <SubmitButton action={this.returnParameters} text="Выполнить"/>
        </Modal.Footer>
      </Modal>
    );
  }
}
