import React from 'react';
import {SubmitButton} from './buttons/submitButton';
import {ContentTable} from './contentTable';
import {PopupForCreateProduction} from './popupForCreateProduction';
import {ProdGenTable} from './productionGeneralInformationTable';
import {PopupForCreateAreaOrWorker} from './popupForCreateAreaOrWorker';
import {PopupForCreateProduct} from './popupForCreateProduct';
import {PopupForCreatePartner} from './popupForCreatePartner';
import {HttpService} from './httpService';

export class ProductionForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: -1,
      data: [],
      headers: [
        {id: 0, name: 'Работники предприятия'},
        {id: 1, name: 'Площадь предприятия'},
        {id: 2, name: 'Продукция предприятия'},
        {id: 3, name: 'Поставщики предприятия'},
        {id: 4, name: 'Потребители предприятия'},
      ],
      idPart: 0,
      isOpenForWorkers: false,
      isUpdateForWorkers: false,
      isUpdateForArea: false,
      isOpenForArea: false,
      isUpdateForProduct: false,
      isOpenForProduct: false,
      isUpdateForPartner: false,
      isOpenForPartner: false,
      isConsumer: false,
      companyService: new HttpService(`company`),
    };
  }

  componentDidMount() {
    this.state.companyService.get()
      .then((result) => {
        console.log(`data = `)
        console.log(result)
        this.setState({
          data: result,
          id: result.length !== 0 ?
            result[0].id : -1,
        });
      });
  }

  openPopupForEditProduction = () => {
    this.setState({isUpdateForProd: true, isOpenForProd: true});
  };

  openPopupForCreateProduction = () => {
    this.setState({isUpdateForProd: false, isOpenForProd: true});
  };

  openPopupForCreateWorker = () => {
    const {idPart} = this.state;
    switch (idPart) {
      case 0:
        this.setState({isUpdateForWorkers: false, isOpenForWorkers: true});
        break;
      case 1:
        this.setState({isUpdateForArea: false, isOpenForArea: true});
        break;
      case 2:
        this.setState({isUpdateForProduct: false, isOpenForProduct: true});
        break;
      case 3:
        this.setState({isUpdateForPartner: false, isOpenForPartner: true, isConsumer: false});
        break;
      case 4:
        this.setState({isUpdateForPartner: false, isOpenForPartner: true, isConsumer: true});
        break;
      default:
        break;
    }
  };

  openPopupForUpdateWorker = () => {
    const {idPart} = this.state;
    switch (idPart) {
      case 0:
        this.setState({isUpdateForWorkers: true, isOpenForWorkers: true});
        break;
      case 1:
        this.setState({isUpdateForArea: true, isOpenForArea: true});
        break;
      case 2:
        this.setState({isUpdateForProduct: true, isOpenForProduct: true});
        break;
      case 3:
        this.setState({isUpdateForPartner: true, isOpenForPartner: true, isConsumer: false});
        break;
      case 4:
        this.setState({isUpdateForPartner: true, isOpenForPartner: true, isConsumer: true});
        break;
      default:
        break;
    }
  };

  closePopupForWorkers = () => {
    const {idPart} = this.state;
    switch (idPart) {
      case 0:
        this.setState({isOpenForWorkers: false});
        break;
      case 1:
        this.setState({isOpenForArea: false});
        break;
      case 2:
        this.setState({isOpenForProduct: false});
        break;
      case 3:
      case 4:
        this.setState({isOpenForPartner: false});
        break;
      default:
        break;
    }
  };

  closePopupForCreateProduction = () => {
    this.setState({isOpenForProd: false});
  };

  selectProduction = (e) => {
    this.setState({id: Number(e.target.value)});
  };

  createProduction = (name, address, phone, fax, industryTypeId, ownershipTypeId) => {
    const {isUpdateForProd, id, data, companyService} = this.state;
    const type = 'default';
    if (!isUpdateForProd) {

      const body = JSON.stringify({name, address, phone, fax, industryTypeId, ownershipTypeId, type});
      companyService.create(body)
        .then((answ) => {
          if (answ.status === 200) {
            answ.json().then((elem) => {
              this.setState({data: [...data, elem], id: elem.id});
            });

          }
        });
    } else {
      const body = JSON.stringify({id, name, address, phone, fax, industryTypeId, ownershipTypeId, type});
      companyService.update(body)
        .then((answ) => {
          if (answ.status === 200) {
            answ.json().then((elem) => {
              const newData = data.filter(e => e.id !== id);
              newData.push(elem);
              this.setState({data: newData});
            });
          }
        });
    }
    this.closePopupForCreateProduction();
  };

  createWorkers = (idAreaOrWorker, value, year, month) => {
    const {isUpdateForWorkers, id, data, idPart, isUpdateForArea, isUpdateForProduct, isUpdateForPartner, companyService} = this.state;
    switch (idPart) {
      case 0:
        if (!isUpdateForWorkers) {
          const type = 'workertype';
          const workertypeId = idAreaOrWorker;
          const body = JSON.stringify({id, workertypeId, value, type});
          companyService.create(body)
            .then((answ) => {
              if (answ.status === 200) {
                answ.json().then((elem) => {
                  const newData = data;
                  newData.filter(e => e.id === id)[0].workers = elem.workers;
                  this.setState({data: newData});
                });
              } else {
                answ.json().then(e => {
                  alert(e.message);
                });
              }
            });
        } else {
          const type = 'workertype';
          const workertypeId = idAreaOrWorker;
          const body = JSON.stringify({id, workertypeId, value, type});
          companyService.update(body)
            .then((answ) => {
              if (answ.status === 200) {
                answ.json().then(() => {
                  const newData = data;
                  newData.filter(e => e.id === id)[0].workers.filter(e => e.id === workertypeId)[0].count = value;
                  this.setState({data: newData});
                });
              }
            });
        }
        break;
      case 1:
        if (!isUpdateForArea) {
          const type = 'areatype';
          const areatypeId = idAreaOrWorker;
          const body = JSON.stringify({id, areatypeId, value, type});
          companyService.create(body)
            .then((answ) => {
              if (answ.status === 200) {
                answ.json().then((elem) => {
                  const newData = data;
                  newData.filter(e => e.id === id)[0].areaTypes = elem.areaTypes;
                  this.setState({data: newData});
                });
              } else {
                answ.json().then(e => {
                  alert(e.message);
                });
              }
            });
        } else {
          const type = 'areatype';
          const areatypeId = idAreaOrWorker;
          const body = JSON.stringify({id, areatypeId, value, type});
          companyService.update(body)
            .then((answ) => {
              if (answ.status === 200) {
                answ.json().then(() => {

                  const newData = data;
                  newData.filter(e => e.id === id)[0].areaTypes.filter(e => e.id === areatypeId)[0].count = value;
                  this.setState({data: newData});
                });
              }
            });
        }
        break;
      case 2:
        if (!isUpdateForProduct) {
          const type = 'producttype';
          const producttypeId = idAreaOrWorker;
          const body = JSON.stringify({id, producttypeId, value, type, year, month});
          companyService.create(body)
            .then((answ) => {
              if (answ.status === 200) {
                answ.json().then((elem) => {
                  const newData = data;
                  newData.filter(e => e.id === id)[0].products = elem.products;
                  this.setState({data: newData});
                });
              } else {
                answ.json().then(e => {
                  alert(e.message);
                });
              }
            });
        } else {
          const type = 'producttype';
          const producttypeId = idAreaOrWorker;
          const body = JSON.stringify({id, producttypeId, value, type});

          companyService.update(body)
            .then((answ) => {
              if (answ.status === 200) {
                answ.json().then(() => {
                  const newData = data;
                  newData.filter(e => e.id === id)[0].products.filter(e => e.id === producttypeId)[0].count = value;
                  this.setState({data: newData});
                });
              }
            });
        }
        break;
      case 3:
        if (!isUpdateForPartner) {
          const type = 'provider';
          const partnerId = idAreaOrWorker;
          const body = JSON.stringify({id, partnerId, type});
          companyService.create(body)
            .then((answ) => {
              if (answ.status === 200) {
                answ.json().then((elem) => {
                  const newData = data;
                  newData.filter(e => e.id === id)[0].providers = elem.providers;
                  this.setState({data: newData});
                });
              } else {
                answ.json().then(e => {
                  alert(e.message);
                });
              }
            });
        }
        break;
      case 4:
        if (!isUpdateForPartner) {
          const type = 'consumer';
          const partnerId = idAreaOrWorker;
          const producttypeId = value;
          const body = JSON.stringify({id, partnerId, type, producttypeId});
          companyService.create(body)
            .then((answ) => {
              if (answ.status === 200) {
                answ.json().then((elem) => {
                  const newData = data;
                  newData.filter(e => e.id === id)[0].consumers = elem.consumers;
                  this.setState({data: newData});
                });
              } else {
                answ.json().then(e => {
                  alert(e.message);
                });
              }
            });
        }
        break;
      default:
        break;
    }
    this.closePopupForWorkers();
  };

  deleteProduction = () => {
    const {data, id, companyService} = this.state;
    const type = 'default';
    const body = JSON.stringify({type, id});
    companyService.deleteWithBody(body)
      .then((answ) => {
        if (answ.status === 200) {
          const newData = data.filter((e) => e.id !== id);
          const currentId = newData.length === 0 ? -1 : newData[0].id;
          this.setState({data: newData, id: currentId});
        }
      });
  };

  deleteItem = (workertypeId) => {
    const {id, data, idPart, companyService} = this.state;
    let type = 'workertype';
    let body = JSON.stringify({type, id, workertypeId});
    let partnerId;
    let producttypeId;

    switch (idPart) {
      case 0:
        companyService.deleteWithBody(body)
          .then((answ) => {
            if (answ.status === 200) {
              const newData = data.filter((e) => e.id === id);
              const wtToUpdate = newData[0].workers.filter(e => e.id !== workertypeId);
              newData[0].workers = wtToUpdate;
              this.setState({data: newData});
            }
          });
        break;
      case 1:
        type = 'areatype';
        const areatypeId = workertypeId;
        body = JSON.stringify({type, id, areatypeId});
        companyService.deleteWithBody(body)
          .then((answ) => {
            if (answ.status === 200) {
              const newData = data.filter((e) => e.id === id);
              const wtToUpdate = newData[0].areaTypes.filter(e => e.id !== workertypeId);
              newData[0].areaTypes = wtToUpdate;
              this.setState({data: newData});
            }
          });
        break;
      case 2:
        type = 'producttype';
        const curProductId = data.filter(e => e.id === id)[0].products.filter(e => e.id === workertypeId)[0].productTypeId;
        producttypeId = curProductId;
        body = JSON.stringify({type, id, producttypeId});
        companyService.deleteWithBody(body)
          .then((answ) => {
            if (answ.status === 200) {
              const newData = data.filter((e) => e.id === id);
              const wtToUpdate = newData[0].products.filter(e => e.id !== workertypeId);
              newData[0].products = wtToUpdate;
              this.setState({data: newData});
            }
          });
        break;
      case 3:
        type = 'provider';
        partnerId = workertypeId;
        body = JSON.stringify({type, id, partnerId});
        companyService.deleteWithBody(body)
          .then((answ) => {
            if (answ.status === 200) {
              const newData = data.filter((e) => e.id === id);
              const wtToUpdate = newData[0].providers.filter(e => e.id !== partnerId);
              newData[0].providers = wtToUpdate;
              this.setState({data: newData});
            }
          });
        break;
      case 4:

        type = 'consumer';
        const curConsumerId = data.filter(e => e.id === id)[0].consumers.filter(e => e.idFake === workertypeId)[0].id;

        partnerId = curConsumerId;
        producttypeId = data.filter((e) => e.id === id)[0].consumers.filter(e => e.id === partnerId)[0].productId;
        body = JSON.stringify({type, id, partnerId, producttypeId});
        companyService.deleteWithBody(body)
          .then((answ) => {
            if (answ.status === 200) {
              //todo mutable!!!! need fix
              const oldData = data;
              console.log(`workertypeid ${workertypeId}`)
              const newConsumers = oldData.filter((e) => e.id === id)[0].consumers.filter(e => e.idFake!==workertypeId);
              oldData.filter((e) => e.id === id)[0].consumers=newConsumers;
              console.log(`new data`)
              console.log(oldData)
              /*const wtToUpdate = newData[0].consumers.filter(e => e.idFake!==workertypeId);
              newData[0].consumers = wtToUpdate;
              */
              this.setState({data: oldData});
            }
          });
        break;
      default:
        break;
    }
  };

  selectPartProduction = (e) => {
    const newIdPart = Number(e.target.value);
    this.setState({idPart: newIdPart});
  };

  getContentByIdPart = () => {
    const {data, id} = this.state;
    if (id === -1) {
      return <div/>;
    }
    const currentProduction = data.find(e => e.id === id);
    switch (this.state.idPart) {
      case 0:
        return this.getItemsByWorkers(currentProduction.workers);
      case 1:
        return this.getItemsByWorkers(currentProduction.areaTypes);
      case 2:
        return this.getItemsByProducts(currentProduction.products);
      case 3:
        return this.getItemsByProviders(currentProduction.providers);
      case 4:
        return this.getItemsByConsumers(currentProduction.consumers);
      default:
        return this.getItemsByWorkers(currentProduction.workers);
    }

  };

  getItemsByWorkers = (workers) => {
    const items = workers.map((e) => (
      {id: e.id, items: [{name: e.name}, {name: e.count}]}
    ));
    return items;
  };

  getItemsByProducts = (products) => {
    console.log(products)
    const items = products.map((e) => (
      {id: e.id, items: [{name: e.name}, {name: e.year}, {name: e.month}, {name: e.count}]}
    ));
    return items;
  };

  getItemsByProviders = (providers) => {
    const items = providers.map((e) => (
      {id: e.id, items: [{name: e.name}, {name: e.isExternal ? e.nameLocation : `Местный`}]}
    ));
    return items;
  };

  getItemsByConsumers = (providers) => {
    const items = providers.map((e) => (
      {
        id: e.idFake,
        items: [{name: e.name}, {name: e.isExternal ? e.nameLocation : `Местный`}, {name: e.productName}],
      }));
    return items;
  };

  getColumnsByIdPart = () => {
    switch (this.state.idPart) {
      case 0:
        return [{name: `Название типа работника`}, {name: `Количество рабочих`}];
      case 1:
        return [{name: `Название типа площади`}, {name: `Квадратных метров`}];
      case 2:
        return [{name: `Название типа продукции`}, {name: `Год`}, {name: `Месяц`}, {name: `Количество`}];
      case 3:
        return [{name: `Название поставщика`}, {name: `Локация`}];
      case 4:
        return [{name: `Название потребителя`}, {name: `Локация`}, {name: `Название продукта`}];
      default:
        return [{name: `Название типа работника`}, {name: `Количество рабочих`}];
    }
  };

  getEditableByIdPart = () => {
    switch (this.state.idPart) {
      case 0:
      case 1:
      case 2:
        return true;
      case 3:
      case 4:
        return false;
      default:
        return false;
    }
  };

  render() {
    const {
      id, data, headers, isOpenForProd,
      isUpdateForProd, isOpenForWorkers, isUpdateForWorkers,
      isOpenForArea, isUpdateForArea, isOpenForProduct, isUpdateForProduct,
      isConsumer, isUpdateForPartner, isOpenForPartner,
    } = this.state;
    if (data.length === 0) {
      return <div align="center">
        <SubmitButton action={this.openPopupForCreateProduction} text="Добавить производство"/>
        <PopupForCreateProduction
          isOpen={isOpenForProd}
          isUpdate={isUpdateForProd}
          closePopup={this.closePopupForCreateProduction}
          performAction={this.createProduction}
        />
      </div>;
    }

    const production = data.find(e => e.id === id);

    return (
      <div>
        <div align="center">
          Текущее предприятие
          <select onChange={this.selectProduction}>
            {data.map((prod) => (
              <option value={prod.id}>{prod.name}</option>
            ))}
          </select>
        </div>


        <div align="center">
          <ProdGenTable
            production={production}
            edit={this.openPopupForEditProduction}
            delete={this.deleteProduction}
          />

          <PopupForCreateProduction
            isOpen={isOpenForProd}
            isUpdate={isUpdateForProd}
            closePopup={this.closePopupForCreateProduction}
            performAction={this.createProduction}
            production={production}
            baseUrl={this.props.baseUrl}
          />

          <br/>

          <SubmitButton action={this.openPopupForCreateProduction} text="Добавить производство"/>

          <br/>

          <br/>
          Какую категорию хотите редактировать
          <select onChange={this.selectPartProduction}>
            {headers.map((elem) => (
              <option value={elem.id}>{elem.name}</option>
            ))}
          </select>


          <PopupForCreateAreaOrWorker
            isOpen={isOpenForWorkers}
            isUpdate={isUpdateForWorkers}
            closePopup={this.closePopupForWorkers}
            performAction={this.createWorkers}
            endpoint='workertype'
            baseUrl={this.props.baseUrl}
          />

          <PopupForCreateAreaOrWorker
            isOpen={isOpenForArea}
            isUpdate={isUpdateForArea}
            closePopup={this.closePopupForWorkers}
            performAction={this.createWorkers}
            endpoint='areatype'
            baseUrl={this.props.baseUrl}
          />

          <PopupForCreateProduct
            isOpen={isOpenForProduct}
            isUpdate={isUpdateForProduct}
            closePopup={this.closePopupForWorkers}
            performAction={this.createWorkers}
            endpoint='producttype'
            baseUrl={this.props.baseUrl}
          />

          <PopupForCreatePartner
            isOpen={isOpenForPartner}
            isUpdate={isUpdateForPartner}
            closePopup={this.closePopupForWorkers}
            performAction={this.createWorkers}
            isConsumer={isConsumer}
            endpoint1='partner'
            endpoint2='producttype'
            baseUrl={this.props.baseUrl}
          />

          <br/>
          <ContentTable content={this.getContentByIdPart()}
                        nameColumn={this.getColumnsByIdPart()}
                        openPopupToUpdate={this.openPopupForUpdateWorker}
                        isEditable={this.getEditableByIdPart()}
                        isDeletable
                        delete={this.deleteItem}/>
          <br/>

          <SubmitButton action={this.openPopupForCreateWorker}
                        text="Добавить"/>

        </div>
      </div>
    );

  }
}
