import React from 'react';
import { withRouter } from 'react-router-dom';
import { HttpService } from './httpService';
import { SubmitButton } from './buttons/submitButton';

class LoginForm extends React.Component {
  state = {
    login: null,
    password: null,
    service: new HttpService(`auth`),
  };

  updateLogin = (e) => {
    this.setState({ login: e.target.value });
  };

  updatePassword = (e) => {
    this.setState({ password: e.target.value });
  };

  doLogin = () => {
    const { login, password, service } = this.state;
    const { action } = this.props;
    const body = JSON.stringify({ login, password });
    service.create(body).then((answ) => {
      if (answ.status === 200) {
        const auth = answ.headers.get('Authorization');
        action(auth);
        this.props.history.push(`/areatype`);
      } else {
        alert(`Неверный логин или пароль, попробуйте снова`);
      }
    });
  };

  render() {
    return (
      <div align="center">
        <div>
          Логин
          <input
            id="login"
            onChange={this.updateLogin}
          />
        </div>
        <br />
        <div>
          Пароль
          <input
            id="password"
            onChange={this.updatePassword}
          />
        </div>
        <br />
        <SubmitButton text="Войти" action={this.doLogin} />
      </div>
    );
  }
}

export const LoginFormWithRouter = withRouter(LoginForm);
