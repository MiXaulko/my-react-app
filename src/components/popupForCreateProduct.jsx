import React from 'react';
import {Modal} from 'react-bootstrap'
import {SubmitButton} from './buttons/submitButton';
import {CancelButton} from './buttons/cancelButton';
import {HttpService} from './httpService';


export class PopupForCreateProduct extends React.Component {

  state = {value: 0, workerType: [], workerTypeId: -1, month: 0, year: 0, service: new HttpService(`producttype`)};

  componentDidMount() {
    this.state.service.get()
      .then((result) => {
        this.setState({
          workerType: result,
          workerTypeId: result[0].id,
        });
      });
  }

  updateValue = (e) => {
    this.setState({value: e.target.value});
  };

  updateYear = (e) => {
    this.setState({year: e.target.value});
  };

  updateMonth = (e) => {
    this.setState({month: e.target.value});
  };

  returnParameters = () => {
    const {value, year, month, workerType} = this.state;
    let {workerTypeId} = this.state;
    if (workerTypeId === -1) {
      workerTypeId = this.state.workerType[0].id;
    }
    this.props.performAction(workerTypeId, value, year, month);
    this.setState({workerTypeId: workerType[0].id});
  };

  selectWorkerType = (e) => {
    this.setState({workerTypeId: Number(e.target.value)});
  };

  render() {
    const {isUpdate, isOpen, closePopup} = this.props;
    const {workerType} = this.state;
    return (
      <Modal show={isOpen} onHide={closePopup}>
        <Modal.Header closeButton>
          <Modal.Title> {isUpdate ? 'Обновить' : 'Добавить'} запись</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <table>
            <tr>
              <td>
                Продукция предприятия
              </td>
              <td>
                <select onChange={this.selectWorkerType}>
                  {workerType.map((elem) => (
                    <option value={elem.id}>{elem.name}</option>
                  ))}
                </select>
              </td>
            </tr>
            <tr>
              <td>
                Количество
              </td>
              <td>
                <input
                  id="value"
                  onChange={this.updateValue}
                />
              </td>
            </tr>
            <tr>
              <td>
                {!isUpdate ? `Год` : ''}</td>
              <td>{!isUpdate ? <input
                id="value"
                onChange={this.updateYear}
              /> : ''}
              </td>
            </tr>
            <tr>
              <td>
                {!isUpdate ? `Месяц` : ''}
              </td>
              <td>
                {!isUpdate ?
                  <input
                    id="value"
                    onChange={this.updateMonth}
                  /> : ''}
              </td>
            </tr>
          </table>
        </Modal.Body>
        <Modal.Footer>
          <CancelButton action={closePopup} text="Отмена"/>
          <SubmitButton action={this.returnParameters} text="Выполнить"/>
        </Modal.Footer>
      </Modal>
    );
  }
}
