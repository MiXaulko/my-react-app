import React from 'react';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import { MenuButton } from './buttons/menuButton';
import { ExitButton } from './buttons/exitButton';

const FixedDiv = styled.div`
  position:fixed;
`;

const RightDiv = styled.div`
   position:absolute;
   top:0;
   right:0;
`;

class SlideMenu extends React.Component {
  state = {
    open: false,
  };

  closeDrawer = () => {
    this.setState({
      open: false,
    });
  };

  openDrawer = () => {
    this.setState({
      open: true,
    });
  };

  doExit = () => {
    sessionStorage.clear();
    this.props.history.push(`/login`);
  };

  render() {
    const { open } = this.state;
    const sideList = (
      <div>
        <List>
          {
            [
              { path: 'areatype', name: 'Тип площади' },
              { path: 'workertype', name: 'Тип рабочего' },
              { path: 'industrytype', name: 'Тип отрасли' },
              { path: 'ownershiptype', name: 'Форма собственности' },
              { path: 'producttype', name: 'Вид продукции' },
              { path: 'partner', name: 'Контрагент' },
              { path: 'production', name: 'Производство' },
            ].map(e => (
              <ListItem button key={e.path}>
                <ListItemText primary={e.name} onClick={() => this.props.history.push(`/${e.path}`)}/>
              </ListItem>
            ))}
        </List>
        <Divider/>
      </div>
    );

    return (
      <div>
        <FixedDiv>
          <MenuButton action={this.openDrawer}/>
          <SwipeableDrawer
            open={open}
            onClose={this.closeDrawer}
            onOpen={this.openDrawer}
          >
            <div
              tabIndex={0}
              role="button"
              onClick={this.closeDrawer}
              onKeyDown={this.closeDrawer}
            >
              {sideList}
            </div>
          </SwipeableDrawer>
        </FixedDiv>
        <RightDiv>
          <ExitButton action={this.doExit}/>
        </RightDiv>
      </div>
    );
  }
}

export const SlideMenuWithRouter = withRouter(SlideMenu);
