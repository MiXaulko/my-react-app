
import React from 'react';
import { Table } from 'react-bootstrap';
import { EditButton } from './buttons/editButton';
import { DeleteButton } from './buttons/deleteButton';

export class PartnerTable extends React.PureComponent {
  render() {
    const { content, nameTable, nameColumn, isDeletable } = this.props;
    return (
      <div>
        <div align="center">
          {nameTable}
        </div>
        <br />
        <Table bordered hover>
          <thead>
            <tr>
              {nameColumn.map((elem) => (
                <td align="center">
                  {elem.name}
                </td>))}
              <td align="center">
                Изменить
              </td>
              {isDeletable &&
                <td align="center">
                  Удалить
              </td>}
            </tr>
          </thead>
          <tbody>
            {content.map(({ id, name, nameLocation }) => (
              <tr key={id}>
                <td align="center">
                  {name}
                </td>
                <td align="center">
                  {nameLocation === null ? 'Местный' : nameLocation}
                </td>
                <td align="center">
                  <EditButton action={() => this.props.openPopupToUpdate(id, name, nameLocation)} />
                </td>
                {isDeletable &&
                  <td align="center">
                    <DeleteButton action={() => this.props.delete(id)} />
                  </td>}
              </tr>
            ))}
          </tbody>
        </Table>
      </div >
    );
  }
}
