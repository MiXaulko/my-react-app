import React from 'react';
import {Modal} from 'react-bootstrap';
import {SubmitButton} from './buttons/submitButton';
import {CancelButton} from './buttons/cancelButton';
import {HttpService} from './httpService';

export class PopupForCreateAreaOrWorker extends React.Component {

  state = {
    value: 0,
    workerType: [],
    workerTypeId: -1,
    service: new HttpService(this.props.endpoint),
  };

  componentDidMount() {
    this.state.service.get()
      .then((result) => {
        this.setState({
          workerType: result,
        });
      });
  }

  updateValue = (e) => {
    this.setState({value: e.target.value});
  };


  returnParameters = () => {
    const {value, workerType} = this.state;
    let {workerTypeId} = this.state;
    if (workerTypeId === -1) {
      workerTypeId = this.state.workerType[0].id;
    }
    this.props.performAction(workerTypeId, value);
    this.setState({workerTypeId: workerType[0].id});
  };

  selectWorkerType = (e) => {
    this.setState({workerTypeId: Number(e.target.value)});
  };

  render() {
    const {isUpdate, isOpen, closePopup} = this.props;
    const {workerType} = this.state;
    return (

      <Modal show={isOpen} onHide={closePopup}>
        <Modal.Header closeButton>
          <Modal.Title> {isUpdate ? 'Обновить' : 'Добавить'} запись</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <table>
            <tr>
              <td>
                Тип площади
              </td>
              <td>
                <select onChange={this.selectWorkerType}>
                  {workerType.map((elem) => (
                    <option value={elem.id}>{elem.name}</option>
                  ))}
                </select>
              </td>
            </tr>
            <tr>
              <td>
                Количество
              </td>
              <td>
                <input
                  id="value"
                  onChange={this.updateValue}
                />
              </td>
            </tr>
          </table>
        </Modal.Body>
        <Modal.Footer>
          <CancelButton action={closePopup} text="Отмена"/>
          <SubmitButton action={this.returnParameters} text="Выполнить"/>
        </Modal.Footer>
      </Modal>
    );
  }
}
