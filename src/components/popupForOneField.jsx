import React from 'react';
import { Modal } from 'react-bootstrap'
import { SubmitButton } from './buttons/submitButton';
import { CancelButton } from './buttons/cancelButton';


export class PopupForOneField extends React.Component {
  state = { newName: '' };

  updateName = (e) => {
    this.setState({ newName: e.target.value });
  };

  returnParameters = () => {
    const { newName } = this.state;
    if (newName !== '') {
      this.props.performAction(newName);
    } else {
      alert('Имя не должно быть пустым');
    }
  };

  render() {
    const { isUpdate, name, isOpen, closePopup } = this.props;
    return (
      <Modal show={isOpen} onHide={closePopup}>
        <Modal.Header closeButton>
          <Modal.Title> {isUpdate ? 'Обновить' : 'Добавить'} строку</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {isUpdate &&
            `Текущая запись ${name}`}
          {isUpdate && <br />}
          <input
            id="name"
            onChange={this.updateName}
          />
        </Modal.Body>
        <Modal.Footer>
          <CancelButton action={closePopup} text="Отмена" />
          <SubmitButton action={this.returnParameters} text="Выполнить" />
        </Modal.Footer>
      </Modal>
    );
  }
}
