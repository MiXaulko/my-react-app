import React from 'react';
import { PopupForPartner } from './popupForPartner';
import { SubmitButton } from './buttons/submitButton';
import { PartnerTable } from './partnerTable';
import { HttpService } from './httpService';

export class PartnerForm extends React.Component {

  state = {
    response: [],
    currentName: '',
    currentNameLocation: '',
    currentId: -1,
    isOpen: false,
    isUpdate: false,
    service: new HttpService(`partner`),
  };

  componentDidMount() {
    this.state.service.get()
      .then((result) => {
        this.setState({
          response: result,
        });
      });
  }

  openPopupForCreate = () => {
    this.setState({ isUpdate: false, isOpen: true });
  };

  openPopupToUpdate = (id, name, nameLocation) => {
    this.setState({
      currentName: name,
      currentNameLocation: nameLocation,
      currentId: id,
      isUpdate: true,
      isOpen: true,
    });
  };

  closePopup = () => {
    this.setState({ isOpen: false });
  };

  performAction = (name, nameLocation) => {
    const { currentId, isUpdate } = this.state;
    if (isUpdate) {
      this.performUpdate(currentId, name, nameLocation);
    } else {
      this.performCreate(name, nameLocation);
    }
    this.closePopup();
  };

  performCreate = (name, nameLocation) => {
    this.create(name, nameLocation);
  };

  performUpdate = (id, name, nameLocation) => {
    this.update(id, name, nameLocation);
  };

  update = (id, name, nameLocation) => {
    const isExternal = nameLocation !== '';
    const object = JSON.stringify({ id, name, isExternal, nameLocation });

    this.state.service.update(object)
      .then((answer) => {
        if (answer.status === 200) {
          const { response } = this.state;
          answer.json().then((elem) => {
            this.setState({ response: [...response.filter(e => e.id !== id), elem] });
          });
        }
      });
  };

  create = (name, nameLocation) => {
    const isExternal = nameLocation !== '';
    const body = JSON.stringify({ name, isExternal, nameLocation });
    this.state.service.create(body).then((answer) => {
      if (answer.status === 200) {
        const { response } = this.state;
        answer.json().then((elem) => {
          this.setState({ response: [...response, elem] });
        });

      }
      else {
        alert('Данная запись уже существует')
      }
    });
  };

  delete = (id) => {
    this.state.service.delete(id).then((answer) => {
      if (answer.status === 200) {
        const { response } = this.state;
        this.setState({ response: response.filter((e) => e.id !== id) });
      }
    });
  };

  render() {
    const { response, isUpdate, isOpen, currentName, currentNameLocation } = this.state;
    const { nameTable, nameColumn, isDeletable } = this.props;
    return (
      <div align="center">
        <PartnerTable
          content={response}
          openPopupToUpdate={this.openPopupToUpdate}
          delete={this.delete}
          nameTable={nameTable}
          nameColumn={nameColumn}
          isDeletable={isDeletable}
        />

        <br />
        <div align="center">
          <SubmitButton action={this.openPopupForCreate} text="Добавить строку" />
        </div>

        <PopupForPartner
          isOpen={isOpen}
          isUpdate={isUpdate}
          name={currentName}
          nameLocation={currentNameLocation}
          closePopup={this.closePopup}
          performAction={this.performAction}
        />
      </div>
    );
  }
}
