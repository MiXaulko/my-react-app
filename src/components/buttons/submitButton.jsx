import React from 'react';
import { Button } from 'react-bootstrap';


export class SubmitButton extends React.PureComponent {
  render() {
    const { action, text } = this.props;
    return (
      <Button variant="info" onClick={action}>
        {text}
      </Button>
    );
  }
}
