import React from 'react';
import DeleteIcon from '@material-ui/icons/Delete';

export class DeleteButton extends React.PureComponent {
  render() {
    return <DeleteIcon onClick={this.props.action} className={DeleteIcon.icon} />;
  }
}
