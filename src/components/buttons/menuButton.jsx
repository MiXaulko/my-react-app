import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

export class MenuButton extends React.PureComponent {
  render() {
    const { action } = this.props;
    return (
      <IconButton color="inherit" aria-label="Open drawer" onClick={action}>
        <MenuIcon />
      </IconButton>
    );
  }
}
