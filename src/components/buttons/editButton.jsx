import React from 'react';
import Edit from '@material-ui/icons/Edit';

export class EditButton extends React.PureComponent {
  render() {
    return <Edit onClick={this.props.action} />;
  }
}
