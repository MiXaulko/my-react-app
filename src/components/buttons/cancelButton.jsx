import React from 'react';
import {Button} from 'react-bootstrap';

export class CancelButton extends React.PureComponent {
  render() {
    const {action, text} = this.props;
    return (
      <Button variant="danger" onClick={action}>
        {text}
      </Button>
    );
  }
}
