import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import ExitToApp from '@material-ui/icons/ExitToApp';

export class ExitButton extends React.PureComponent {
  render() {
    const { action } = this.props;
    return (
      <IconButton color="inherit" aria-label="Open drawer" onClick={action}>
        <ExitToApp />
      </IconButton>
    );
  }
}
