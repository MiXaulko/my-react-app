import React from 'react';
import { Table } from 'react-bootstrap';
import { EditButton } from './buttons/editButton';
import { DeleteButton } from './buttons/deleteButton';

export class ContentTable extends React.PureComponent {
  render() {
    const { content, nameTable, nameColumn, isDeletable, isEditable } = this.props;
    return (
      <div>
        <div align="center">
          {nameTable}
        </div>
        <Table bordered hover>
          <thead>
            <tr>
              {nameColumn.map((elem) =>
                (<td align="center">
                  {elem.name}
                </td>))}
              {isEditable &&
                <td align="center">
                  Изменить
              </td>}
              {isDeletable &&
                <td align="center">
                  Удалить
              </td>}
            </tr>
          </thead>
          <tbody>
            {content.map(({ id, items }) => (
              <tr key={id}>
                {items.map(item => (
                  <td align="center">
                    {item.name}
                  </td>))}
                {isEditable &&
                  <td align="center">
                    <EditButton action={() => this.props.openPopupToUpdate(id, items)} />
                  </td>}
                {isDeletable &&
                  <td align="center">
                    <DeleteButton action={() => this.props.delete(id)} />
                  </td>}
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    );
  }
}
