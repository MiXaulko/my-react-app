
import React from 'react';
import { Table } from 'react-bootstrap'
import { EditButton } from './buttons/editButton';
import { DeleteButton } from './buttons/deleteButton';

export class DictionaryTable extends React.PureComponent {
  render() {
    const { content, nameTable, nameColumn, isDeletable } = this.props;
    return (
      <div>
        <div align="center">
          {nameTable}
        </div>
        <br />
        <Table bordered hover>
          <thead>
            <tr>
              {nameColumn.map((elem) => (
                <td align="center">
                  {elem.name}
                </td>))}
              <td align="center">
                Изменить
              </td>
              {isDeletable &&
                <td align="center">
                  Удалить
                </td>}
            </tr>
          </thead>
          <tbody>
            {content.map(({ id, name }) => (
              <tr key={id}>
                <td align="center">
                  {name}
                </td>
                <td align="center">
                  <EditButton action={() => this.props.openPopupToUpdate(id, name)} />
                </td>
                {isDeletable &&
                  <td align="center">
                    <DeleteButton action={() => this.props.delete(id)} />
                  </td>}
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    );
  }
}
