import React from 'react';
import { Table } from 'react-bootstrap';
import { EditButton } from './buttons/editButton';
import { DeleteButton } from './buttons/deleteButton';

export class ProdGenTable extends React.PureComponent {
  // todo need refactor or delete class and use dictionary table may be
  render() {
    const { production } = this.props;
    return (
      <div>
        <Table bordered hover>
          <thead>
            <tr>
              <td align="center">
                Адрес
              </td>
              <td align="center">
                Телефон
              </td>
              <td align="center">
                Факс
              </td>
              <td align="center">
                Тип отрасли
              </td>
              <td align="center">
                Форма собственности
              </td>
              <td align="center">
                Изменить
              </td>
              <td align="center">
                Удалить
              </td>
            </tr>
          </thead>
          <tbody>
            <tr key={production.id}>
              <td align="center">
                {production.address}
              </td>
              <td align="center">
                {production.phone}
              </td>
              <td align="center">
                {production.fax}
              </td>
              <td align="center">
                {production.industryType}
              </td>
              <td align="center">
                {production.ownershipType}
              </td>
              <td align="center">
                <EditButton action={() => this.props.edit()} />
              </td>
              <td align="center">
                <DeleteButton action={() => this.props.delete()} />
              </td>
            </tr>
          </tbody>
        </Table>
      </div>
    );
  }
}
