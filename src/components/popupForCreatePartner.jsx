import React from 'react';
import {SubmitButton} from './buttons/submitButton';
import {CancelButton} from './buttons/cancelButton';
import {Modal} from 'react-bootstrap';
import {HttpService} from './httpService';


export class PopupForCreatePartner extends React.Component {
  constructor(props) {
    super(props);
    this.getPartner();
    this.getProductTypes();
  }

  state = {
    workerType: [], workerTypeId: -1, products: [], productTypeId: -1,
    service1: new HttpService(this.props.endpoint1),
    service2: new HttpService(this.props.endpoint2),
  };

  returnParameters = () => {
    const {workerTypeId, productTypeId, workerType, products} = this.state;
    const {isConsumer} = this.props;
    if (isConsumer) {
      this.props.performAction(workerTypeId, productTypeId);
    } else {
      this.props.performAction(workerTypeId);
    }
    this.setState({workerTypeId: workerType[0].id, productTypeId: products[0].id});
  };

  getPartner = () => {
    this.state.service1.get()
      .then((result) => {
        this.setState({
          workerType: result,
          workerTypeId: result[0].id,
        });
      });
  };

  getProductTypes = () => {
    this.state.service2.get()
      .then((result) => {
        this.setState({
          products: result,
          productTypeId: result[0].id,
        });
      });
  };

  selectPartner = (e) => {
    this.setState({workerTypeId: Number(e.target.value)});
  };

  selectProduct = (e) => {
    this.setState({productTypeId: Number(e.target.value)});
  };

  render() {
    const {isUpdate, isOpen, closePopup, isConsumer} = this.props;
    const {workerType, products} = this.state;
    return (
      <Modal show={isOpen} onHide={closePopup}>
        <Modal.Header closeButton>
          <Modal.Title> {isUpdate ? 'Обновить' : 'Добавить'} запись</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <table>
            <tr>
              <td>
                Поставщик предприятия
              </td>
              <td>
                <select onChange={this.selectPartner}>
                  {workerType.map((elem) => (
                    <option value={elem.id}>{elem.name}</option>
                  ))}
                </select>
              </td>
            </tr>
            <tr>
              <td>
                {isConsumer ? 'Продукт' : ''}
              </td>
              <td>
                {isConsumer ?
                  <select onChange={this.selectProduct}>
                    {products.map((elem) => (
                      <option value={elem.id}>{elem.name}</option>
                    ))}
                  </select>
                  : ''}
              </td>
            </tr>
          </table>
        </Modal.Body>
        <Modal.Footer>
          <CancelButton action={closePopup} text="Отмена"/>
          <SubmitButton action={this.returnParameters} text="Выполнить"/>
        </Modal.Footer>
      </Modal>
    );
  }
}
