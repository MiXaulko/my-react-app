import React from 'react';
import { Grid } from '@material-ui/core';
import { PopupForOneField } from './popupForOneField';
import { SubmitButton } from './buttons/submitButton';
import { DictionaryTable } from './dictionaryTable';
import { HttpService } from './httpService';

export class DictionaryForm extends React.Component {

  state = {
    response: [],
    currentName: '',
    currentId: -1,
    isOpen: false,
    isUpdate: false,
    service: new HttpService(this.props.url),
  };

  componentDidMount() {
    this.state.service.get()
      .then(result => {
        this.setState({
          response: result,
        });
      });
  }

  openPopupForCreate = () => {
    this.setState({ isUpdate: false, isOpen: true });
  };

  openPopupToUpdate = (id, name) => {
    this.setState({ currentName: name, currentId: id, isUpdate: true, isOpen: true });
  };

  closePopup = () => {
    this.setState({ isOpen: false });
  };

  performAction = (name) => {
    const { currentId, isUpdate } = this.state;
    if (isUpdate) {
      this.update(currentId, name);
    } else {
      this.create(name);
    }
    this.closePopup();
  };

  update = (id, name) => {
    this.state.service.update(JSON.stringify({ id, name }))
      .then((answer) => {
        if (answer.status === 200) {
          const { response } = this.state;
          answer.json().then((elem) => {
            this.setState({
              response: [...response.filter(e => e.id !== id), elem].sort((a, b) => {
                return a.id - b.id;
              }),
            });
          });
        }
      });
  };

  create = (name) => {
    this.state.service.create(JSON.stringify({ name }))
      .then((answer) => {
        if (answer.status === 200) {
          const { response } = this.state;
          answer.json().then((elem) => {
            this.setState({ response: [...response, elem] });
          });
        }
      },
      );
  };

  delete = (id) => {
    this.state.service.delete(id)
      .then((answer) => {
        if (answer.status === 200) {
          const { response } = this.state;
          this.setState({ response: response.filter((e) => e.id !== id) });
        }
      });
  };

  render() {
    const { response, isUpdate, isOpen, currentName } = this.state;
    const { nameTable, nameColumn, isDeletable } = this.props;
    return (
      <div>

        <DictionaryTable
          content={response}
          openPopupToUpdate={this.openPopupToUpdate}
          delete={this.delete}
          nameTable={nameTable}
          nameColumn={nameColumn}
          isDeletable={isDeletable}
        />

        <br />
        <div align="center">
          <SubmitButton action={this.openPopupForCreate} text="Добавить строку" />
        </div>

        <PopupForOneField
          isOpen={isOpen}
          isUpdate={isUpdate}
          name={currentName}
          closePopup={this.closePopup}
          performAction={this.performAction}
        />

      </div>
    );
  }
}
